# Gite nerden başlamalıyım ?

Git/gitlab ile çalışmaya başlamadan önce bir eklenti ve bir uygulama indirmemiz gereklidir. Bunun için ilk önce 
[git download](https://git-scm.com/download/win) sitesinden işletim sisteminize uygun versiyonu indirmeniz gerekmektedir. Kurulumunda fazladan ayar yapmadan kurmanız yeterlidir. Daha sonra ise eclipse marketplaceden Egit plugini kurmanız gerekmektedir. Burada da stantdart bir kurulum yeterli olacaktır.

![](../img/Egitplugin.PNG) 

Artık git ve gitlab da çalışmak için hazırız.

# Artık Proje Değil Repository

Git ile çalışmaya başlamadan önce bazı terimleri bilmemiz gerekiyor. Bunların en başında gelen **Repository** projelerimiz artık git tarafından izlendiğinden pjolerimiz artık birer repository oluyor bu ne demek ne gibi farklılıklar var gibi soruların cevabı daha fazla teknik detay gerektirir. 
Bir repositorinin içerisinde çok fazla **branch** olabilir. Bunların hepsi projenin çeşitli zamanlarında alınmış kopyası olarak düşünebilirsiniz. 