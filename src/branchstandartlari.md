# Standartlar

## Branch isimlendirme standartı

branchler amaçlarına uygun olarak bir çok isim alabiriler. Genellikle işlerin amaçları ile isimlendirdiğimiz branchler ayrıca işin nevinide tam olarak nitelemelidir.

çalışma ortamımızda brachler için şimdilik iki prefix belirledik. Bunlardan birisi **feature** diğeri ise **fix** yani biz bir branh oluştururken prefix olarak bu ikisinden birisini mutlaka kullanacağız. Daha sonra işi tanımlayan bir başlık sonrasında ise işin jazz id olmak üzere branch isimlendirmemizi tamamlayacağiz.

*örnek:* **feature/isin_adi_jazid** ==> **feature/tesvik_hata_duzelme_ekrani_12345**

*örnek:* **fix/hata_adi_jazid** ==> **fix/donem_borc_goruntuleme_ekrani_düzeltme_123456**

### Ne zaman fix ne zaman feature?

feature branchi oluşturmak için bize bir iş tanımlanması ve bu işin mevcut sisteme yeni bir özellik ekleme şeklinde olması gerekir yani özellik tamamlandıktan sonra test edilmeli testleri olumlu ise prod ortamına alınmalı. Fix branchi ise mevcut sistemde bir hatanın olduğu zamanlar ani mudahale gerektiren işlemlerde açılır. Bu branchin test edilmeye ihtiyacı yoktur. Ancak eşitlik bozulmaması için test brachinede gönderilmelidir.