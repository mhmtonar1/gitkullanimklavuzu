
# BRANCHS

## Branchleri görüntüleme

Branch tanımı [burada](http://gitlab/siydb/isveren-sistemi/GitKullanimKlavuzu/tree/master#art%C4%B1k-proje-de%C4%9Fil-repository) yapmıştık. Çalışma prensipleri ile ilgili detaylar için git-scm sitesi kontrol edilebilir svn-git farkına da ulaşabilirsiniz. Buradaki amacımız eclipse ortamında brachleri tanımak uzaktan bir branchi nasıl alabileceğimiz görmek silinmesi yeniden adlandırılması gibi temel işlemler olacak.

Eclipse de brachleri görüntülemek için projemizde(repository) sağ tıklayıp sırası ile **Team > Switch to > Other** dememiz yeterli daha sonra karşımıza aşağıdaki gibi bir ekran çıkacaktır.

**Team > Switch to > Other**

![File > import > git](../img/dallar2.PNG) 

 bu ekrandaki en önemli iki klasör **Local** ve **Remote Tracking** dir local klasörü altında olan dallar bizim bilgisayarımızda mevcut olan dallardır. Remote Tracking de olanlar ise bizim uzak sunucuda olduğunu bildiğimiz dallardır. 

 ### UYARI 1 

 Diyelim ki projeyi clone ettiniz çalışmaya başladınız bir süre sonra arkadaşınıza bir iş verildi arkadaşınız kendisine o iş için brach açtı ve sonra işi size devretti. 
 
 Sizin arkadaşınızın çalıştığı brache devam etmeniz gerekiyor. Ancak branchleri açtığınız da arkadaşınızın branchinin sizin **Remote Tracking** de olmadığını fark edeceksiniz. Bunun nedeni ise sizin arkaşınız branchi açtıktan sonra repositoryi **fetch** etmemenizdir. Yani git size şunu diyor senin izlediğin uzaktaki brachler artık değişti yeniden fetch işlemi yapmazsan ben onu sana gösteremem diyor

 ### Çözüm

 projeye yeni eklenmiş dalları görmek için repositoryi fetch etmemiz yeterli olacaktır. Bunun için ise **Team > Remote > Configure fetch from upstream** dememiz gerekiyor. Bu ekranda neleri fetch edeceğimizi göreceğiz.

![File > import > git](../img/fetch.PNG) 

yukarıda sarı ile işaretlediğim yer uzak sunucudan hangi dalları fetch ettiğimdir. ben * yazdığım için **Save and Fetch** dediğim anda uzaktaki tüm brachleri fetch edecektir. Burada sadece talep ettiğiniz dallar da olabilirdi. Burada dikkat etmeniz gereken bir diğer nokta ise fetch etmeye çalıştığınız dalın hala uzakta oluyor olmasıdır. Eğer siz özel bir dalı fetch etmeye çalıştınız ancak bu dal uzakta artık mevcut değilde eclipse hata verecektir. Hataya baktınız anda zaten detaylarını verecektir.

## Uzaktan fetch ettiğimiz bir branchi localde oluşturma

uzaktan fetch ettiğimiz branchi localde kopyasını oluşturmak için ilk yapmamız gerek iş [burada](http://gitlab/siydb/isveren-sistemi/GitKullanimKlavuzu/blob/master/src/brachs.md#branchleri-g%C3%B6r%C3%BCnt%C3%BCleme) tarif ettimiz üzere brachlerinizi görüntülemenizdir. **Remote Tracinking** de bizim fetch ettiğimiz brachler mevcut. Localde kopyasını oluşturmak istediğimiz branchin üzerine çift tıklayarak aşağıdaki ekranı açabiliriz. 

![File > import > git](../img/checkout.PNG) 

boyalı butonu seçerek sıradaki ekrana geçebilirsiniz.

![File > import > git](../img/newdal.PNG) 

bu ekranda en önemli konu branchi hangi uzak brachten refere ettiğinizdir. **origin/feature/robotAciklamaEkleme** diyerek uzaktaki bu branchi localde **feature/robotAciklamaEkleme** adı ile kullanacağım demektir.

### UYARI 2

uzaktaki bir bracnhi locale alırken asla ismini değiştirmeyin bu çalışma ile ilgili bir sorun oluşturmayacaktır ancak projede çok fazla branch olduğundan uzaktaki hangi brach localde nasıl temsil ediliyor karıştırabilirsiniz.

### UYARI 3 

uzaktaki branchin localde temsilcisini oluşturma o branchi asla uzaktan koparmaz yani siz localdeki **feature/robotAciklamaEkleme** branchine push ettiğiniz anda uzak temsilcisi olan **origin/feature/robotAciklamaEkleme**a gönderecektir. 


## Branch isim değiştirme yada Silme işlemleri

bir brachin ismini yanlış oluşturmuş olabiliriz. İsmini değiştirmek için **Team > Advenced > Rename Branh**  dememiz yeterli olacaktır. Aşağıdaki gibi bir ekran açılacağından kolaylıkla değiştirilebilir.

![](../img/rename.PNG)

burada dikkat etmeniz gereken husus uzaktaki bir branchin ismini değiştirmeyin bunu yapmak istiyorsanızda gitlabın sitesi üzerinden yapınız

Branchi silmek içinde yine aynı yolu izleyebilirsiniz. Burada da yine uzaktaki branchi silmemeye çalışın.