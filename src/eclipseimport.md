# Projenin eclipse ortamına import edilmesi

Herhangi bir workspacede çalışırken gitlabdan projeyi import edebiliriz bunun için gitlabdaki repository url yi bilmemiz gerekiyor.

Bu urle ulaşmak için gitlabdaki projenizi açıp clone butonuna tıklayıp çıkan linki kopyalayın
![](../img/clone.PNG) 

daha sonra eclipse gelip projeyi import etmek gerekiyor. File menüsü altında import alt menüsüne tıklayım oradan git paketinden import from git(smart) olanı seçiniz

**File > import > git > Projects from git(with smart import)**

![File > import > git](../img/importgit.PNG) 

Daha sonra karşınıza aşağıdaki gibi bir ekran gelecektir. Buradaki konu şu siz bir repository import etmek istiyorsunuz ancak bu repository url den mi import edeceksiniz yoksa daha önce import ettiniz bu proje bilgisayarınızda var onu mu import edelim diyor. Biz ilk defa projeyi import edeceğimiz için clone URI seçeneğini seçiyoruz


![File > import > git](../img/selectrepo.PNG) 

Clone URI seçeneğini seçtikten sonra karşımıza aşağıdaki ekran gelecektir.

![File > import > git](../img/auth.PNG) 

 ileri seçip devam ediyoruz.

 ![File > import > git](../img/dallar.PNG) 

 buradaki olayımız ise şudur bizim burada hangi **branch**leri  fetch etmek isteğimizi soruyor burada dikkat edilmesi gereken en önemli konu ise **fetch** etmek demek uzaktaki brachi localimize almak demek değildir. sadece uzaktaki bu brachlerden haberdarım demektir. Egit varsayılan olarak master brachini local brache kopyalayacaktır. ancak diğer brachleri locale almayacaktır. bunu daha sonra kendimiz yapmalıyız.

  ![File > import > git](../img/nereye.PNG) 

  bu ekranda ise repositornin nereye kaydedileceğini seçmemiz gerekiyor bu klasör workspace ile aynı ekran olmak zorunda değildir.

  ileri seçeneğini seçerek proje kullanıma  hazırdır.




