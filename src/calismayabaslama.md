# Git ile çalışmaya başlama

Eğer buraya kadar gelmişseniz kurulumları tamamlamış, projenizi clone etmiş, local brach remote brach farkını anlamış olmanız gerekiyor. 

**herhangi bir işe başlamadan önce ilk yapmamız gereken işlem localdeki master branchimizi uzaktaki master branchi ile güncellmektir** bu işlemi yapmanız için **Team > pull** demeniz yeterlidir. Daha sonra ise localdeki bu master branchimizi kullanarak işmiz ile ilgili olan brachi oluşturacağız bunun için ise 
**Team > switch to > other** dememiz gerekiyor. Karşımıza aşağıdaki gibi bir ekran çıkacak

![](../img/new.PNG)

burada dikkat etmemiz gereken en önemli konu yeni oluşturacağımız branche referans olarak localdeki master branchini almamızdır. Local klasörü altında master seçmeliyiz. Master üzerine tek tıklayıp **new branch** dediğimiz zaman karşımıza aşağıdaki ekran çıkacaktır.

![](../img/new2.PNG)

burada branchimize yeni isim vereceğiz. [Buradan](../src/branchstandartlari.md) branch isimlendirme standartlarına bakabilirsiniz.

branchimize isim verip bitir dediğimizde çalışmaya başladığımız branche bizi taşıyacaktır artık branchimiz yaratılmış olarak görüntülenecektir. bu branch şuan sadece localimizdedir. Uzak sunucuda bu branchi görüntüleyemeyeceksiniz. 

buraya kadar yaptığımız işi özetlersek local olan master branchinden kendimize yeni bir branch oluşturduk. 

Branchimzide yapmak istediğimiz değişikleri yapıp bunları commit etmemiz gerekiyor. Bundan önce iki terimi detaylandıralım.

**commit** => bir değikliği commit yapmak demek olan değişikliği gite eklemek demektir. bu işlemi localde yaparsanız yani eclipsde yaptığınız değişikliği localinize eklemişsiniz demektir. uzak sunucuya gitmez.

**push** =>  push ise commit topluluğunu uzağa göndermek demektir. Bir branchi push edebilirsiniz bu işlem branchinizdeki değişiklikleri uzaktaki aynı branche yazar eğer branh yoksa oluşturur.

## Ne zaman commit? Ne zaman Push?

yaptığınız değişiklikleri sık sık commit etmekte fayda var bu işlem sizin kendinize ön gördüğünüz kilometre taşları ile ilgilidir. bir jsp sayfasının tasarımını bitirip commit edebilirsiniz. Ancak push ise daha major değişiklikleri kapsar push demek artık localde işim bitti bunu uzağa at orada yapacağım işlemlere devam edeceğim demek

## Branchi push etme

Yapacağımız işlemler tamam olduğunda yani bize gelen iş ile ilgili geliştirmelerimizi tamamladığımızda test edilmesi için değişikliklerimizi test brachine taşımamız gerekiyor bunun için ilk yapmamız gereken işlem branchi uzak sunucuya göndermek. En son commit işlemini de tamamladıktan sonra branchi uzak sunucuya göndermek için **Team > Push branch > 'branch isminiz'** tıklıyoruz. 
bu sayede branhimizi gitlab sayfasında görüntüleye bileceğiz.

## Branchi uzak sunucuda görüntüleme

Push ederek sunucuya gönderdiğimiz branch projenizin menüsünde **Repository > Branches**'de görüntülenmektedir. 

![](../img/gitlabb.PNG)

calistiğiniz branch ile ilgili iki tane seçenek görüntülenmektedir. Bunlardan birisi **Merge request** diğeri **Compare** merge request işlemi sizin branchinizdeki değişiklikleri herhangi bir branch ile birleştirme ilemi demektir. Compare ise branchler arası karşılaştırma yapar. 

## Merge Request

Üsteki resimden **Merge Request** butonuna tıkladığımız zaman karşımıza aşağıdaki gibi bir pencere açılacaktır.

![](../img/mg.PNG)

sarı ile işaretlenen ilk yer hangi brachden hangi branche birleşme olacağını sectiğimiz yerdir. Change branch diyerek birleştirme isteğini başka branche alabiliriz.
 
 sarı ile işaretlenen ikinci kısım ise birleştirme isteğini kimin onaylayacağıdır.

 ## Test ve prod

Çalıştığımız branch ile ilgili işlemlerimiz bittiğinde çalışmalarımızı test edilmesi için ilk önce test branchine daha sonra prod branchine taşıyoruz. Burada dikkat edilmesi gereken en önemli nokta hiçbir branch asla test branhine gönderilmeden prod branchine gönderilmemesidir. Aksi takdirde confligler artacaktır.
