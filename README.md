# Amaç

Bilindiği üzere projelerin git/gitlab ortamına taşınması ile ilgili talimat bulunmaktadır. Bu kapsamda git ile nasıl çalışılması gerektiği büyük bir sorun olmuştur. Dağınık bir sistemde bir standardın olmayısı ise mevcuttaki karışıklığı daha da artırmaktadır. Bunun için git ile çalışmaya başlamadan önce buradaki yazılanların dikkate alınması belirtilen standartlara uyulması zorunluluk haline gelmiştir. 

# **Git ile çalışırken yapılacak işlemler aşağıdaki gibi gruplanmıştır**

> **:construction: [Eclipse Git kurulum](./src/kurulum.md) :wrench:**

> **:arrow_down: [Eclipse projenin locale çekilmesi](./src/eclipseimport.md)**

> **:cactus: [Branchleri keşfedelim](./src/brachs.md)**

> **:construction_worker: [Çalışmaya Başlama](./src/calismayabaslama.md)**(bir işe nasıl başlanması gerektiğine dair bilgiler içerir)

> **:notebook: [Branch isimlendirme standartları](../src/branchstandartlari.md)**

> **:new: [İlk defa oluşturulan projenin gitlabda paylaşılması](../src/paylas.md)**